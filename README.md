[![pipeline status](https://gitlab.com/infomir-lib/adonis-websocket/badges/develop/pipeline.svg)](https://gitlab.com/infomir-lib/adonis-websocket/commits/develop)
[![coverage report](https://gitlab.com/infomir-lib/adonis-websocket/badges/develop/coverage.svg)](https://gitlab.com/infomir-lib/adonis-websocket/commits/develop)

# adonis websocket server

This fork add the ability to the adonis websocket server to start multiple websocket server using the same http server has defined in [multiple-servers-sharing-a-single-https-server](https://github.com/websockets/ws#multiple-servers-sharing-a-single-https-server)

## usage
replace `@adonisjs/websocket` package by

```json
"@adonisjs/websocket": "git+ssh://git@gitlab.com:infomir-lib/adonis-websocket.git#v1.0.11"
```

in your `package.json`

add a `start/hooks.js` file with the fallowing content

```js
'use strict'

const { hooks } = require('@adonisjs/ignitor')

/**
 * WebSocket servers init
 */
hooks.after.httpServer(() => {
  const url = require('url')
  const httpServer = use('Server').getInstance()

  const wsServer = use('Ws')
  wsServer.listen()

  /**
   * init ws server per respective path
   */
  httpServer.on('upgrade', function upgrade (req, socket, head) {
    switch (url.parse(req.url).pathname) {
      case wsServer.path:
        wsServer.handleUpgrade(httpServer, req, socket, head)
        break
        
      default:
        socket.destroy()
        break
    }
  })
})
```

> This configuration ensure that each websocket server will upgrade incoming request only if for its respective uri.
 

## references

* [websocket/ws](https://github.com/websockets/ws)
* [adonis-websocket](https://github.com/adonisjs/adonis-websocket)




